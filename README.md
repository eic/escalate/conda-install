# conda install

Script to automatically install Escalate framework stack using miniconda.

**Escalate with miniconda is not tested on macs yet!**

## Install Escalate

Install with 2 lines:

```bash
# Go to directory, where you would like to install escalate
wget https://gitlab.com/eic/escalate/conda-install/raw/master/install_escalate.py
python3 install_escalate.py
```


> (!) Conda is open source and is released under the 3-clause BSD license, you agree with it automatically if you run the script (!)


By default it will create a directory `escalate` in current folder. Ejana and g4e can be found under the
same folder. Packets like root and geant are downloaded and installed by conda.

To start using the installation, source one of:

```bash
souce escalate/escalate.sh    # for bash
souce escalate/escalate.csh   # for csh
```

Run jupyter with examples:

```bash
souce escalate/escalate.sh
cd escalate/workspace
jupyter lab
```

Run g4e or ejana (from command line)
```bash
g4e
ejana
```

Run ejana or g4e in console

Installation folder might be controlled by `ESCALATE_TOP_DIR` environment variable.

```bash
#bash
ESCALATE_TOP_DIR=<mydir> python3 install_escalate.py
```

## Troubleshooting CERN Root install

By default this script installs root from binaries using conda.
On some operating systems (Ubuntu 16.04 machine) there was conda conflict installing python and root at the same time. 
To resolve this, add a flag ```--build-root```

```bash
python3 install_escalate.py --build-root
```

This will make script to build CERN Root from sources from EJPM. This will take additional time but mitigate the issue. 



## Procs and cons

Why one need this type of installation over the docker or bare ejpm? 

1. Docker provides fully configured system, where everything works out of the box. Still it is a box.
In case one just need to process a MC file - docker is just OK. But if one needs to change code or
to work with graphics (opengl event viewers and stuff like this), then docker becomes less convenient. 

    On linux docker is light wrapper but on macOS - it is a light VM. One can also assume sligtly better
     performance than with docker 

2. [ejpm](https://gitlab.com/eic/escalate/ejpm) allows to build packages directly on your machine. 
While ejpm automates build process, it can't really control system configuration and dependencies, 
so a user has to ensure that there is C++17 compatible compiler (for ACTS tracking), packages like QT
are installed and so on. This is OK in general for experts but may not work in too many cases.

3. Conda provides its own environments. Which ships required compilers, and dependencies which guarantees
consistency. Still packets are built and run on your system. Thus it almost eliminates weak spots of docker and ejpm.
  

## What the script does?

1. Download and miniconda
2. Create conda environment with name 'esc'
3. Download (conda install) precompiled root, geant, hepmc2, clhep and other binary packages
4. Build g4e and ejana
5. Configure jupyterlab extensions
6. Generate user frendly environemnt script


